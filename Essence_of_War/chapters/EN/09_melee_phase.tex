\section{MELEE PHASE}
\EWquote{A spray of arterial blood brought me back to my senses as the warrior hit our line, passing through our ranks like a shadow made of serrated steel. There was nothing but red ruin in his wake.}
{Report by venerable runic smith Gavan on the disastrous loss of life at the final battle of Avran's Bay}

During each Melee Phase, all the ongoing Combats fight a Round of Combat. The Active Player decides the order of the Combats. A Combat is defined as a group of opposing units, which are all connected through base contact. Normally, this would be two units fighting against one another, but it could also be several units against a single enemy unit or a long chain of units from both sides.%

\begin{multicols}{2}

\subsection{Round of Combat Sequence}
Each Round of Combat is divided into the following steps:%

\begin{tabular}{c|p{0.38\textwidth}}
1 & Start a unit's combat\\
2 & \characters{} may Swap Places\\
3 & Determine Initiative Order and who can Attack\\
4 & Roll Melee Attacks, higher Agility first\\
 & -- Allocate Attacks, Roll To-Hit, To-Wound, Saves, and remove casualties\\
 & -- Repeat 4.1 for the next initiative step\\
5 & Calculate Combat Result. Losers roll Break Test\\
6 & If the Loser fails the Break Test:\\
 & -- Roll Panic Tests\\ 
 & -- Winner decides whether to Pursue\\
 & -- Roll Flee (and Pursuit) distances\\
 & -- Move Fleeing (and Pursuing) units \\
7 & Perform Combat Pivots\\
8 & Proceed to the next combat\\
\end{tabular}\\

\subsection{Initiative Order}
Each Round of Combat is fought in a strict striking order, referred to as Initiative Order. The Initiative Order is determined before any attacks are made, taking into account all modifiers that affect the Agility of attacks that may be performed in this Round of Combat. Once the Initiative Order has been determined for a Round of Combat, it cannot be changed. The order starts at Initiative Step 10 with all attacks with Agility 10, and then working down from the highest to Initiative Step 0 with all attacks with Agility 0.%

At each Initiative Step, all attacks from this step strike simultaneously (see Which Models can Attack, below) strike simultaneously; complete the attacks for both sides before removing any casualties.%

\subsection{Which Models can Attack}
Models in base contact with an enemy attack during their Initiative Step. This includes models in direct base contact, models \hyperref[section:FightingOverGaps]{\textit{Fighting over Gaps}} and models able to perform \hyperref[section:Supporting_Attacks]{\textit{Supporting Attacks}}. Models from both sides attack in each player's Melee Phase.%

\subsubsection*{Supporting Attacks}\label{section:Supporting_Attacks}
Models in the second rank can perform one single attack across models in the first rank. This is called a Supporting Attack. Supporting Attacks can only be made against enemies in the front facing. A model can only ever make a single supporting attack, regardless of their Attacks characteristics.%

\subsubsection*{Fighting over Gaps}\label{section:FightingOverGaps}
Sometimes, there are gaps in a \closecombat{}. If two units are in base contact and at least one of them has a front facing towards the engaged enemy, models in these units are allowed to attack over empty gaps, but not across other units or \impassableterrain{}. These models are considered to be in base contact with each other.%

%% need to add correct figure
\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{diagrams/FIGURE_EW_Melee_Strike_Over_Gaps.png}
\caption{
	Unit A is not Engaged in its front, so the models cannot make Supporting Attacks.%
	\\
	Unit B: models in the first rank count as being in base contact with an enemy and can attack (including across the empty gap). The models in the second rank can only perform Supporting Attacks. The rest of the Models cannot attack at all.
	}
\label{fig:EW_Fighting_Through_Gaps}
\end{figure}

\subsection{Allocate and Roll Melee Attacks}

At each \Agility{} step, before any attacks are rolled, attacks must first be allocated against enemy models. If a model is in base contact with more than one model, it can choose which model to attack. For example, a model in base contact with regular models and \characters{} can choose to strike one or the other, or split attacks between the two groups.%

The number of attacks that a model can make is equal to its \AttackValue{}. Model rules, spells and other effects can further change this number. If a model has more than one attack, it can allocate them at will to different targets in base contact. If a model is making Supporting Attacks, it can allocate its single attack as if it was in the front of its unit in the same file. If a model could either strike at models in base contact or make Supporting Attacks, it must allocate its attacks against models in base contact. Players must allocate all attacks at a given \Agility{} step before rolling any To-Hit rolls.%

\subsubsection*{Rolling to Hit}
To make Hit rolls, subtract the \DefensiveSkill{} of the target from the \OffensiveSkill{} of the attacking model and roll a \Dsix{}. Consult the chart below to see what result is required based on this difference.
%
\begin{center}
\begin{tabular}{r|c}
Attacker's \textbf{\OffensiveSkillInitials{}} & Required%
\tabularnewline
minus Target's \textbf{\DefensiveSkillInitials{}} & to \textbf{Hit}%
\tabularnewline
\hline
4 or more & 2+%
\tabularnewline
1 to 3 & 3+%
\tabularnewline
0 to -3 & 4+%
\tabularnewline
-4 to -7 & 5+%
\tabularnewline
-8 or less & 6+%
\tabularnewline
\hline
\end{tabular}
\end{center}

If one or more hits are scored, follow the procedure described under \hyperref[section:GeneralPrinciples_Attacks]{\textit{Attacks}} (see page \pageref{section:GeneralPrinciples_Attacks}).%

\begin{center}
\includegraphics[width=.35\textwidth]{art/ORNAMENT_046_sword+map.png}
\end{center}

\subsubsection*{Dropping out of Combat}
Removing casualties may cause units to drop out of base contact with their foe, for example in the case of rear or flank charges. When this happens, units are nudged back into combat the way a warrior would close the gap a fallen comrade leaves behind. At the end of each \Agility{} step, starting with the unit that suffered the least casualties, move the unit the minimum distance needed to keep base contact between the Engaged Units. Units that are \hyperref[section:ChargePhase_EngagedInCombat]{\textit{Engaged in Combat}} with more than an enemy unit are never nudged in this way. If it is not possible to keep base contact between the units, the unit drops out of \closecombat{}. Any units that are no longer Engaged in Combat follow the rules given under \hyperref[section:MeleePhase_NoMoreFoes]{\textit{No More Foes}} on page \pageref{section:MeleePhase_NoMoreFoes}.%

\subsection{Combat Result}

Once all \Agility{} steps have passed and all models have had a chance to attack, the winner of this Round of Combat is determined. This is done by calculating each side's Combat Score. To calculate Combat Scores, add up all Combat Score bonuses from the list below. The side with the higher Combat Score wins the combat, the side with the lower Combat Score loses the combat. If there is a tie, both sides are treated as winners.%

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item \textbf{Lost \HealthPoints{} on enemy units: \plusone{} for each Health Point}. Each player adds up the number of \HealthPoints{} their opponents lost during this Round of Combat. This includes units that were Engaged in the Combat but dropped out or were completely wiped out during this Round of Combat. Only include units engaged in the same combat.%
\item \textbf{Rank Bonus: \plusone{} for each rank (max. +3)}. Each side adds \plusone{} to their Combat Score for each rank after the first in a single unit, up to a maximum of +3. Only count this for a single unit per side, use the unit that gives the highest Rank Bonus. Only count ranks with at least one model remaining after removing casualties.%
\item \textbf{Charge Bonus: +1}. Each side with one or more Charging models receives \plusone{} to their Combat Score.%
\item \textbf{Flank or Rear Bonus: +2}. Each side adds +2 to their Combat Score if they have one or more units fighting an enemy in the enemy's flank or rear.%
\end{itemize}

\subsubsection*{Break Tests}\label{section:MeleePhase_BreakTest}
Units that lost the Round of Combat must take a Break Test in an order chosen by the losing player. A Break Test is a \Discipline{} Test with a negative modifier to their \Discipline{} equal to the Combat Score difference. For example, a unit with \Discipline{} 7 scores 1 Combat point and the enemy scores 3 Combat points. The unit on the losing side takes Break Tests with a \minus{2} modifier to its \Discipline{}, thus with a \Discipline{} 5. If the test is failed, the unit Breaks and Flees, and friendly units take \hyperref[section:GeneralPrinciples_Panic]{\textit{Panic Tests}} (see page \pageref{section:GeneralPrinciples_Panic}). If the test is passed, the unit remains Engaged in the Combat. Their resolve holds and they can continue fighting!%

\subsubsection*{Steadfast}\label{section:MeleePhase_Steadfast}
Any units that have more ranks than each of the enemy units Engaged in the same Combat ignore \Discipline{} modifiers from the Combat Score difference. A unit cannot use the Steadfast rule if it is affected by Broken Ranks (see \hyperref[section:GeneralPrinciples_HinderingTerrain]{\textit{Hindering Terrain}}, page \pageref{section:GeneralPrinciples_HinderingTerrain}), or if its flank or rear is Engaged in Combat with an enemy unit with at least 2 ranks. Remember that units consisting of a single model (e.g. \characters{} or \rnf{} units reduced to a single model) have zero ranks.%

\subsubsection*{No More Foes}\label{section:MeleePhase_NoMoreFoes}
Sometimes a unit kills all enemy units in base contact and finds itself no longer Engaged in Combat. These units always count as winning the combat, and can perform either a \hyperref[section:MovementPhase_Pivots]{\textit{Pivot}} (see page \pageref{section:MovementPhase_Pivots}) or an \hyperref[section:MeleePhase_Overrun]{\textit{Overrun}} if they just Charged (see page \pageref{section:MeleePhase_Overrun}).%

When this happens in combats involving multiple units, the \HealthPoints{} loss caused to and by the unit are counted towards the Combat Score, but all other Combat Score bonuses are ignored. Note, that the unit itself does not need to take a Break Test -- it always counts as being on the winning side.%

\subsection{Flee and Pursuit}
Before moving broken units, enemy units that are in base contact with any broken units may declare a Pursuit of a single broken unit. To be able to pursue a broken unit, the pursuing unit cannot be \hyperref[section:ChargePhase_EngagedInCombat]{\textit{Engaged}} with any non-broken enemy units and must be in base contact with the broken unit. Units can elect not to pursue. In this case, they may either perform a \hyperref[section:MovementPhase_Pivots]{\textit{Pivot}} (see page \pageref{section:MovementPhase_Pivots}) or remain stationary.%

\begin{figure}[H]
\centering
\includegraphics[width=0.47\textwidth]{diagrams/FIGURE EW Pursuit.png}
\caption{
	a) The single model Unit A (dark) Breaks from combat. It Pivots to face away from the enemy Unit B (light) that won the combat, and then moves the Flee Distance rolled: forward \distance{6}.%
	\\
	b) The winning Unit B (light) Pursues. It Pivots to face the same direction as the Fleeing Unit A, and then moves the Pursuit Distance rolled: forward \distance{3}.%
	}
\label{fig:EW_Pursuit}
\end{figure}

\subsubsection*{Roll for Flee and Pursuit Distance}
Every broken unit now rolls \XDsix{2} to determine their Flee Distance, and each unit that has declared a Pursuit now rolls 2D6 to determine its Pursuit distance. The unit is no longer Engaged in Combat. If any pursuing unit rolls an equal or higher Pursuit Distance than the Flee Distance of the unit it is pursuing, the fleeing unit is destroyed on the spot and removed from the game.%

\subsubsection*{Flee Distance and Fleeing Units}
Each broken unit not captured and destroyed will now flee directly away from an enemy unit in base contact chosen by the player that won that Combat Round. Once it has been established which unit the Flee Move will be away from, Pivot the fleeing unit so that its Rear Facing is in contact with the enemy unit it is fleeing from. Move the fleeing unit forward a number of inches equal to the flee distance rolled earlier. Use the rules for \hyperref[section:GeneralPrinciples_Flee]{\textit{Flee Move}} (see page \pageref{section:GeneralPrinciples_Flee}). If several units are fleeing from the same combat, the player controlling the fleeing units chooses which order they roll the flee distance, and the units will move in the same order.%

\begin{center}
\includegraphics[width=.4\textwidth]{art/ORNAMENT_066_skulls_sword.png}
\end{center}

\subsubsection*{Pursuit Distance and Pursuing Units}
Each pursuing unit now performs two Pursuit Moves: a Pivot to face the pursuing direction, and an Advance forward by the pursuit distance rolled. Pursuit Moves are not affected by friendly units part of the same combat, which are treated like Open Terrain.%

The Board Edge, \impassableterrain{}, enemy units that Fled from the combat involving the Pursuing unit, and friendly units that were not part of that combat are considered Pursuit Obstructions. If a Pursuit Move would bring the unit to overlap first with a Pursuit Obstruction, interrupt that Pursuit Move so the unit stops \distance{1} from the Obstruction.%

If a Pursuit Move would bring the unit to overlap first with a different enemy unit's Unit Boundary, the pursuing unit declares a charge against that enemy unit. Determine which Arc the Pursuing unit is Located in. Remove the pursuing unit from the Battlefield and then place it back on the Battlefield with its Front Facing in base contact with its target, in the enemy facing determined previously, maximising the number of Engaged models as normal but keeping the Centre of the unit as close as possible to its starting position while doing so. The Charged unit may only Hold as Charge Reaction (even if already fleeing, see \hyperref[section:ChargePhase_ChargingFleeingUnit]{\textit{Charging a Fleeing Unit}}, page \pageref{section:ChargePhase_ChargingFleeingUnit}).%

Once the Pursuit Moves have been completed, check if the pursuing unit is in a legal position. It cannot be in base contact with a unit it didn't declare a Charge against, and it must be at least \distance{1} away of any unit or obstacle (see \hyperref[section:Armies_UnitSpacing]{\textit{Unit Spacing}}, page \pageref{section:Armies_UnitSpacing}). If the unit is not in a legal position, backtrack the move to the unit's last legal position.%

\subsubsection*{Overrun}\label{section:MeleePhase_Overrun}
A unit that charged into combat this turn and has no enemy units left in base contact after the Combat Round, whether from combat or any other effect, can choose to make a special Pursuit Move called Overrun instead of a Pivot. Overruns follow the rules for moving pursuing units, except that the direction of the Pursuit Move is always straight-forward.%

\subsection{Combat Pivot}\label{section:MeleePhase_CombatPivot}
After Fleeing and Pursuing units have moved, each unit still \hyperref[section:ChargePhase_EngagedInCombat]{\textit{Engaged in Combat}} can perform a Combat Pivot. This manoeuvre can be used to bring more models into melee or, for example, to reform and face the enemy after having endured a flank charge, or to slide the unit sideways to be closer to an objective or evade possible enemy charge.%
\\

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item Units Engaged in more than one Facing (e.g. in both Front and a Flank) can never perform any Combat Pivots.%
\item Units on the losing side of the combat have to pass a \Discipline{} Test in order to do so.%
\item After all Combat Pivot \Discipline{} tests have been taken, the Active Player decides which player performs their Combat Pivot first. This player must complete all Combat Pivots with their units, one at a time, in any order. Then the other player can perform Combat Pivots for their units.%
\item Each player may choose not to Combat Pivot one or more of their units.%
\end{itemize}

When performing a Combat Pivot, remove a unit from the Battlefield and place it back, following the following restrictions:%

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item The unit cannot move into base contact with enemy units that it was not in base contact with before the Combat Pivot.%
\item The unit must be placed in base contact with the same enemy units as it was before the Combat Pivot, and in contact with the same Facing of the enemy units.%
\item \characters{} that were in base contact with an enemy must still be after the Combat Pivot. This applies to both enemy and friendly \characters{}. A Character may end up in base contact with different enemy models than it was before the Combat Pivot.%
\item After each Combat Pivot, there must be at least as many models of the Combat Pivoting unit in base contact with enemy models as there were before. These don't have to be the same models. Furthermore, after a player has completed all their Combat Pivots, exactly the same enemy models that were in base contact with opposing models before the Combat Pivot must still be in base contact after the Combat Pivot (but they may be Engaged with different models or even units).%
\end{itemize}

\end{multicols}
