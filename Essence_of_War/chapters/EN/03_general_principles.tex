\section{GENERAL PRINCIPLES}
\EWquote{In principle, I generally find that the most principled generals \\ have the poorest understanding of general principles.}
{Duke D'Auberge, in writing}
\begin{multicols}{2}

\subsection{\characters{}}\label{section:GeneralPrinciples_Characters}

\characters{} are represented in a Patrol or Armylist by a crowned skull icon. All \characters{} can operate individually. In this case, follow the normal rules for units composed of a single model i.e. it has no ranks, and can make any number of \hyperref[section:MovementPhase_Pivots]{\textit{Pivots}} during Advance and March Moves (see page \pageref{section:MovementPhase_Pivots}).%

However, \characters{} can also operate as part of other units, by joining them and creating a Combined Unit. A Character can only join a unit by deploying the Character in the unit during Deploying Units in the Pre-Game Phase. A Character can only deploy in a unit like this if it has the exact same base size as the unit's models.%

Units with 1 or more ranks in their army list description can be joined by a single Character. A Character joining a unit replaces a rank and file unit model in the first rank. Remove that regular unit model from the game to maintain a consistent unit boundary footprint.%

\begin{center}
\includegraphics[width=0.3\textwidth]{art/ORNAMENT_062_skull_candles.png}
\end{center}

Once joined to a unit, the Character is considered as part of the Combined Unit for all rules purposes, and they cannot leave the unit. Note that \characters{} do not gain any model rules (such as \shootingweapons{}) that the regular unit models have when in a Combined Unit.%

\subsubsection*{Swap Places}\label{section:GeneralPrinciples_SwapPlaces}

If a Combined Unit is not Engaged in combat, \characters{} can swap places with another model in the first rank in the unit during the Movement Phase.%

If a Combined Unit is Engaged in combat, \characters{} from the Active Player may swap places at the beginning of the Melee phase, but must always be placed in the front rank and in base-to-base contact with an enemy model if they do so.%

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{diagrams/FIGURE EW Join Unit.png}
\caption{
	a) A \wizard{} (dark square) chooses to deploy in a unit of 10 Handgunners. One Handgunner is removed from the unit (skull).
	\newline
	b) The combined unit is deployed with 9 Handgunners and 1 \wizard{}.
	\newline
	c) During the game, the \wizard{} is removed as casualty.
	\newline
	d) A Handgunner from the rear rank fills the gap left.
}
\label{fig:EW_Basic_Join_Unit}
\end{figure}

\subsubsection*{Distributing Hits}\label{section:GeneralPrinciples_DistributingHits}

When an Attack hits a Combined Unit that does not allow specific models to be targeted, such as most Spells or Shooting Attacks, the hits are all resolved against \rnf{} models. The only way in which a Character in a Combined Unit can be attacked is either by first eliminating all the \rnf{} models, or by allocating attacks specifically onto the Character.%

If a Combined Unit has all its \rnf{} models killed leaving a Character behind, the remaining Character is still considered to be the same unit for ongoing effects (such as Lasts one Turn spells) and Panic (no unit is considered destroyed)%

\subsection{Attacks}\label{section:GeneralPrinciples_Attacks}

Whenever an attack hits a model, use the following sequence. Complete each step for all the attacks that are happening simultaneously (such as all Shooting Attacks from a single unit or all \closecombat{} Attacks at the same \Agility{} step) before moving on to the next step.%

\begin{tabular}{c|p{0.38\textwidth}}
1 & Attacker rolls to wound.%
\tabularnewline
2 & Defender rolls \Armour{} Saves.%
\tabularnewline
3 & Defender rolls \aegis{} Saves.%
\tabularnewline
4 & Defender suffers unsaved wounds and removes Health Points or casualties.%
\tabularnewline
5 & Defender checks for Panic.%
\end{tabular}

\subsubsection*{To-Wound Rolls}

If an attack has a \Strength{} value, it must successfully wound the target to have a chance to harm it. An attack with \Strength{} 0 cannot wound. Compare the \Strength{} of the attack to the \Resilience{} Characteristic of the target by subtracting the \Resilience{} value from the \Strength{} value of the attack and consulting the chart below.%

The player that inflicted the hit makes a To-Wound roll for each attack that hit the target. An unmodified roll of \result{6} will always succeed and an unmodified roll of \result{1} will always fail. If the attack does not have a \Strength{} value, follow the rules given for that particular attack. Roll a \Dsix{} for each hit. To find out what score is needed to successfully wound the target, see the table below.%

\begin{center}
\begin{tabular}{r|c}
Attack's \textbf{\StrengthInitials{}} & Required%
\tabularnewline
minus Target's \textbf{\ResilienceInitials{}} & to \textbf{Wound}%
\tabularnewline
\hline
2 or more & 2+%
\tabularnewline
1 & 3+%
\tabularnewline
0 & 4+%
\tabularnewline
-1 & 5+%
\tabularnewline
-2 or less & 6+%
\tabularnewline
\hline
\end{tabular}
\end{center}

\subsubsection*{\Armour{} Saves}

If one or more wounds are inflicted, the player whose unit is being wounded now has a chance to save the wound(s) if it has any \Armour{}. The \Armour{} Value is reduced by the \ArmourPenetration{} of the enemy attack causing the wound. To make an \Armour{} Save Roll, roll a \Dsix{} for each wound and consult the table below.%

A natural roll of \result{1} will always fail. If the \Armour{} Save is passed the wound is disregarded.%

\begin{center}
\begin{tabular}{r|c}
Target's \textbf{\ArmourInitials{}} & Required%
\tabularnewline
minus Attack's \textbf{\AP{}} & to \textbf{Save}%
\tabularnewline
\hline
0 or less & No save possible%
\tabularnewline
1 & 6+%
\tabularnewline
2 & 5+%
\tabularnewline
3 & 4+%
\tabularnewline
4 & 3+%
\tabularnewline
5 or more & 2+%
\tabularnewline
\hline
\end{tabular}
\end{center}

\subsubsection*{\aegis{} Saves}

The attacked model has a final chance to disregard a wound that was not saved by its \Armour{} Save, provided it has an \aegis{} Save. \aegis{} saves may be indicated either in the Characteristics Profile or as \aegis{X+} in Model Rules.%

To make an \aegis{} Save, roll a \Dsix{} for each wound that was not saved by the model's \Armour{} Save. If the result is equal or higher than the value indicated in brackets for that model's \aegis{} save, the wound is discarded. If the \aegis{} Save is failed (or if the model had no \aegis{} Save) an unsaved wound is caused.%

If the model or unit has access to more than one \aegis{} value, choose which one to use before rolling.%

\subsubsection*{Losing \HealthPoints{}}

For each unsaved wound, the unit being attacked loses a Health Point.%

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item \textbf{Rank and File models}: Non-Character models in the same unit share a common Health Pool. %
If the attack was against non-Character models, their combined Health Pool loses 1 Health Point for each unsaved wound. Models are removed as casualties when they reach 0 \HealthPoints{}. If the models have 1 Health Point each, remove one model for each lost Health Point. If the models have more than 1 Health Point each, remove whole models whenever possible. Keep track of \HealthPoints{} lost that are not enough to kill an entire model (e.g. by placing markers next to wounded models or by using a die). These lost \HealthPoints{} are taken into account for future attacks. If the unit is wiped out, any excess lost \HealthPoints{} is ignored%
\\

For example, a unit of 6 Ogres (3 \HealthPoints{} each) loses 7 \HealthPoints{}. Remove two whole models (6 \HealthPoints{}), leaving 1 Health Point leftover which is kept track of with a die or token.%Later, this unit loses 2 \HealthPoints{}, which is enough to kill a single Ogre since 1 Health Point was lost from the previous attack. If the unit is wiped out, any excess lost \HealthPoints{} is ignored.%
\\
\item \textbf{Character models}: Character models can only lose \HealthPoints{} when they are specifically targeted by an attack (for example, by allocating \closecombat{} attacks towards them). If so, the attacked model loses 1 Health Point for each unsaved wound, and is removed as casualty when reaching 0 \HealthPoints{}. If the model is killed, any excess inflicted \HealthPoints{} are ignored.%
\end{itemize}

\subsubsection*{Removing Casualties}\label{section:GeneralPrinciples_RemovingCasualities}

Casualties are removed from anywhere on the rear most rank. If the unit has a single rank, remove models as equally as possible from both sides of the unit. If the unit is Engaged in Combat, remove the models in a way that the number of units (first priority) and number of models (second priority) in base contact is maximised. Note that the requirement to remove casualties equally from both sides of a single rank unit only applies to each batch of simultaneous attacks.%

If a Character is standing in a position that would normally be removed as a casualty, remove the next eligible \rnf{} model and move the non-Character(s) inwards. Character casualties are removed from their positions within the unit directly. Other models are then moved to fill in empty spots. When doing this, the models follow the same guidelines as for casualty removal stated above.%

\begin{center}
\includegraphics[width=0.3\textwidth]{art/ORNAMENT_063_rats.png}
\end{center}

\subsection{\Discipline{} Test}

There are many different game mechanics that may call for a \Discipline{} Test, such as performing a Panic Test or a Break Test. All such game mechanics are classified as \Discipline{} Tests.%

To perform a \Discipline{} Test the player nominates a model to use their \Discipline{} to hold the unit together, this is typically the model with the highest \Discipline{} value in the unit, such as when a Character joins a unit. The Player now rolls \XDsix{2} and compares the result with the \Discipline{} Characteristic of the model. If the roll result is equal or less than the \Discipline{} value, the test is passed. Otherwise, the test is failed.%

\subsection{Panic}\label{section:GeneralPrinciples_Panic}

Panic Tests are \Discipline{} Tests taken immediately after any of the following situations arise:%

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item A friendly unit is destroyed within \distance{6}, including fleeing off the board, in any Phase. Measure from the destroyed unit's Unit Boundary as it was at the start of the Phase in which the unit was destroyed, or the unit's position when it touched the Board Edge.%
\item A friendly unit Breaks from combat within \distance{6}. Measure from the breaking unit's Unit Boundary before the unit makes its \hyperref[section:GeneralPrinciples_Flee]{\textit{Flee Move}}.
\item A friendly unit Flees through the unit's Unit Boundary. Test once the Fleeing unit has completed its move.%
\item The unit suffers, in a single Magic or Shooting Phase, Health Point losses equal to or greater than 25\% of the number of \HealthPoints{} that it had at the start of the Phase. Units that started the game as a single model (as specified on the Army List), do not take Panic Tests from this.%
\end{itemize}

Units do not take Panic Tests if they are Engaged in Combat, if they are already Fleeing or if they already passed a Panic Test earlier during that Phase.%

Units which fail a Panic Test due to a friendly unit being destroyed, fleeing through its unit or Breaking from combat flee directly away from the closest enemy unit.%

Units which fail a Panic Test in the Magic or Shooting phase due to Health Point losses flee directly away from the unit that caused the Panic Test (unless that unit is no longer in the battlefield).%

Determine the direction by drawing a line from the Centre of the enemy unit being fled from through the Centre of the panicking unit and Pivot the panicking unit around its Centre, so that the middle point of its Rear Facing is along this line (for more details on Pivots, see page \pageref{section:MovementPhase_Pivots}). Then immediately make a Flee Move with that unit using the rules below.%
\\

\subsection{Flee Move}\label{section:GeneralPrinciples_Flee}

To perform a Flee Move, roll \XDsix{2} to determine the Flee Distance. Move the fleeing unit this distance straight forward.%

If this move should make the fleeing unit end its move in contact with (or within \distance{1} from) another unit's Unit Boundary or \impassableterrain{}, extend the Flee Distance with the minimum distance needed for the unit to get \distance{1} away from such obstructions (see \hyperref[section:Armies_UnitSpacing]{\textit{Unit Spacing}}, page \pageref{section:Armies_UnitSpacing}). Remember that units take Panic Tests if a friendly unit flees through their Unit Boundary.%

If the Flee Move takes the fleeing unit into contact with or beyond the Board Edge, the unit is destroyed. Remove the unit as a casualty as soon as it touches the Board Edge.%

When a unit is fleeing, it cannot perform any action, including: Declare Charges, Move (other than Flee), Shoot or Cast Spells. The Active Player can attempt to Rally Fleeing Units in the \hyperref[section:MovementPhase_MovementPhase]{Movement Phase} (see page \pageref{section:MovementPhase_MovementPhase}).%

\subsection{Terrain}

\textit{A traditional army might feel at ease fighting battles on open plains. Its units need space to move around and get into position. They might get stuck or blocked if they have to manoeuvre around ruins, large rocks, or pits. But because a general cannot always choose the battlefield, he learns how to fight in all types of terrain.}\\

A Terrain Feature is an area of the Battlefield that grants specific rules and effects to units or models. All parts of the battlefield are divided into one of the following categories:%

\subsubsection*{Open Terrain}

\textit{Some commanders agree to meet each other in the open field, so they can display their skills of war without external interference.}\\

Open Terrain has no effect upon Line of Sight or Movement. All parts of the board that are not covered by any other kind of Terrain are considered to be Open Terrain.%

\subsubsection*{Elevated Terrain}

\textit{The elevated position of a hill or ziqqurat gives a natural advantage to anyone atop it.}

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item \textbf{Line of Sight}: Can be drawn onto and down from Elevated Terrain, but not through it. Note that Line of Sight is still blocked by intervening \impassableterrain{}.%
\item \textbf{Cover}: Units with more than half of their Target Facing partially obscured by Elevated Terrain benefit from Cover. When determining Cover to or from models on Elevated Terrain, ignore all intervening models which are not on Elevated Terrain themselves. %
\item \textbf{Movement}: The movement of models is unaffected by Elevated Terrain.%
\end{itemize}

\vfill\null
\columnbreak

\subsubsection*{Hindering Terrain}\label{section:GeneralPrinciples_Hindering_Terrain}

\textit{The rules for Hindering Terrain can be used to represent all forms of terrain impeding movement, such as dense forests, old ruins and treacherous marshlands.}

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item \textbf{Line of Sight}: Can be drawn through Hindering Terrain.%
\item \textbf{Cover}: Units with more than half of their Target Facing inside and/or behind Hindering Terrain benefit from Cover.%
\item \textbf{Dangerous Terrain}: The movement of models with bases of 20\timess{}20mm or 25\timess{}25mm is unaffected by Hindering Terrain. Models with bases larger than 25x25mm that march, charge, flee, pursue or overrun into, out of or through a piece of Hindering Terrain must perform a Dangerous Terrain test. Roll a \Dsix{} for each model. If a \result{1} is rolled, the model suffers a hit that wounds automatically and has \ArmourPenetration{} 10. These wounds do not cause \hyperref[section:GeneralPrinciples_Panic]{\textit{Panic}} (see page \pageref{section:GeneralPrinciples_Panic}).%
\item \textbf{Broken Ranks}: Units with the majority of its bases' area inside Hindering Terrain can never be \hyperref[section:MeleePhase_Steadfast]{\textit{Steadfast}} (see page \pageref{section:MeleePhase_Steadfast}).%
\end{itemize}

\subsubsection*{\impassableterrain{}}

\textit{\impassableterrain{} represents terrain that cannot be moved through, such as closed off buildings, steep cliffs or huge boulders.}

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item \textbf{Line of Sight}: Cannot be drawn through \impassableterrain{}.%
\item \textbf{Cover}: Units with more than half of their Target Facing partially obscured by Impasable Terrain benefit from Cover.%
\item \textbf{Movement}: Models cannot move into or through \impassableterrain{}, and must remain at least \distance{1} away such terrain (except during a Charge Move).
\item \textbf{Flee and Pursuit}: If a Flee movement ends with the Unit Boundary on an \impassableterrain{}, extend the Flee move accordingly. Pursuing models must stop pursuing upon reaching \distance{1} away such terrain. See page \pageref{section:GeneralPrinciples_Flee}.%
\\~\\
\end{itemize}

\end{multicols}

\begin{center}
\includegraphics[width=0.7\textwidth]{art/ORNAMENT_005_watercolor_line.png}
\end{center}
