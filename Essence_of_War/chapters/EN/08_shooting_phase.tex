\section{SHOOTING PHASE}
\EWquote{My personal preference is for fighting Johnny Ogre while he's still very far away.}
{Captain Samantha Keller of the Imperial Artillery}

\begin{multicols}{2}
In the Shooting Phase, models with Shooting Attacks get a chance to use them.%

\subsection{Round Sequence}
The Shooting Phase is divided into the following steps.%
\begin{tabular}{c|p{0.38\textwidth}}
2 & Select a unit to perform a Shooting Attack\\
3 & Determine the Aim value and resolve the Attack\\
4 & Repeat step 2 with a different unit\\
\end{tabular}\\

\subsection{Performing a Shooting Attack}
Some units have \shootingweapons{} or Model Rules that allow them to perform Shooting Attacks. Apply the following rules for shooting with a unit:%

\subsubsection*{Choose a shooting unit and its target}
Each of your units with \shootingweapons{} can attempt to perform a Shooting Attack. The shooting weapons state the Range, Aim, number of shots, \Strength{}, and \AP{} of the attack, as well as any potential additional rules. Units that are Fleeing, Engaged in Combat, or that have Marched, Pivoted (unless a single model unit), Rallied, or Declared a Charge in their previous Movement Phase cannot shoot.%

When a unit shoots, first nominate a target unit within the shooting unit's \hyperref[section:PreparingForWar_LineOfSight]{\textit{Line of Sight}}. A unit is considered to have shooting Line of Sight to a target if one or more models in the unit have Line of Sight to it. Units that are Engaged in Combat cannot be chosen as targets. All models in the same unit must shoot at the same target.%

\subsubsection*{Choose models to shoot with}
Only models in the first and second rank of a unit may shoot. Check Line of Sight and Range for each individual model. A model can only shoot if the target is within its Line of Sight and within the Range of its weapon. Range is measured from the shooting model to the closest point of the target's Unit Boundary (even if this particular point is not within Line of Sight). Once this has been established, these models shoot as many times as indicated in their weapon's profile.%

For each shot, roll to hit for each model and each shot based on the models Aim modifiers, as described below. If one or more hits are scored, follow the procedure described under \hyperref[section:GeneralPrinciples_Attacks]{\textit{Attacks}} on page \pageref{section:GeneralPrinciples_Attacks}. If casualties are caused a \hyperref[section:GeneralPrinciples_Panic]{Panic Test} (see page \pageref{section:GeneralPrinciples_Panic}) may be required.%

\subsection{Aim}
All \shootingweapons{} have an Aim written in brackets after the weapon's name. The Aim tells you what the model needs to roll on a \Dsix{} to successfully hit its target. This roll is called a To-Hit roll.%

Note that the Aim is not bound to the weapon, instead each unit has its own Aim for a given \shootingweapon{} available to it. For example, an elven archer might have a \longbow{} (3+) while a human archer only has a \longbow{} (4+). The elf would hit its target if it rolls 3 or higher on a \Dsix{}, while the human would need to roll a 4 or higher.%

\begin{center}
\includegraphics[width=.47\textwidth]{art/ORNAMENT_064_skulls_arrows.png}
\end{center}

\subsubsection*{Aim Modifiers}
Shooting Attacks may suffer one or more Aim modifiers to their To-Hit rolls. For each modifier applicable, add \plusone{} penalty to the Aim of the weapon, and thus reducing the chances of success. For example, an elven archer with a \longbow{} (3+) shooting at Long Range (\plusone{} Aim penalty) will hit its target with a roll of 4+. If the To-Hit roll would be modified to 7+ or worse, then the model cannot hit the target. An unmodified roll of \result{1} always counts as failed.%

\begin{center}
\begin{tabular}{rl}
\hline
Long Range & \plusone{}%
\tabularnewline
Moving and Shooting & \plusone{}%
\tabularnewline
Cover & \plusone{}%
\tabularnewline
\hline
\end{tabular}
\end{center}

\subsubsection*{Long Range (\plusone{} Aim Modifier Penalty)}
All \shootingweapons{} have a Range written in the weapon's profile. If the target is further away than half of the weapon's Range, the shooting model receives a \plusone{} Aim modifier penalty.%

\subsubsection*{Moving and Shooting (\plusone{} Aim Modifier Penalty)}
If the unit has moved during this Player Turn, all models in the unit receive a \plusone{} Aim modifier penalty.%

\subsubsection*{Cover (\plusone{} Aim Modifier Penalty)}
Cover is determined individually for each shooting model. Determine if the target benefits from Cover as follows:%
\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item Determine which Arc of the target the shooting model is located in. The corresponding Facing is referred to as Target Facing.%
\item From any point on the shooting model's Front Facing, check how large the fraction of the Target Facing is that is obscured.%
\end{itemize}

Apply Cover if at least 50\% of the target's Facing is obscured by other units, \impassableterrain{} or Elevated terrain, or is inside and/or behind Hindering Terrain.%

Models always ignore their own unit and the Terrain Feature they are inside. For example, a model shooting from the second rank of a unit within Hindering Terrain does not suffer an Aim modifier due to Cover for shooting through that Hindering Terrain.%

\begin{figure}[H]
\centering
\includegraphics[width=0.45\textwidth]{diagrams/FIGURE EW Cover.png}
\caption{
	Example of Cover inside Hindering Terrain.\\
	a) The left model in the shooting Unit A is Located in the Flank Arc of unit B, so Unit B's Flank Facing is the Target Facing. More than half of the Target Facing is obscured inside the Hindering Terrain, so Unit B benefits from Cover against the left model.%
	\\
	b) The right model in Unit A is Located in the Front Arc of Unit B, so Unit B's Front Facing is the Target Facing. Less than half of the Target Facing is obscured inside the Hindering Terrain, so Unit B does not benefit from Cover against the right model.%
	}
\label{fig:EW_Cover}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=0.45\textwidth]{diagrams/FIGURE EW Cover Combo.png}
\caption{
	Example of Cover from blocked Line of Sight.\\
	Unit A is shooting at Unit C. More than half of the Target Facing of Unit C is obscured by other Unit Boundaries and terrain that blocks Line of Sight. In this case the target counts as benefiting from Cover.%
	}
\label{fig:EW_Cover_Combo}
\end{figure}

\end{multicols}
